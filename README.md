## How to use the vivoclientapi in your application?

The API is available in a maven repository, so you just need to add simple relevant configuration.

Add the repository  https://bitbucket.org/sssvivo/vivoclientapi/raw/master/repository to your Android project build.gradle like this:

```
allprojects {
    repositories {
        jcenter()

        maven {
            url 'https://bitbucket.org/sssvivo/vivoclientapi/raw/master/repository/'
        }
    }
}
```

Then in your module/application build.grade

Add the following: `compile 'ch.supsi.netlab:vivoclientapi:0.1'` in the dependencies section.

You can replace 0.1 with the version you would like to use.

Currently only version 0.1 is available.
## How to publish a new version of the Library to the Git repository?

Change the version of the library in https://bitbucket.org/sssvivo/vivoclientapi/src/master/app/build.gradle

Run the task `gradle uploadArchives` from the root of the project.

It will generate a new version into https://bitbucket.org/sssvivo/vivoclientapi/src/master/repository/ch/supsi/netlab/vivoclientapi/
Make sure all the generated files are committed and push to git.

