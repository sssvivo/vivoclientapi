package ch.supsi.netlab.vivoclientapi.privacy.localdp;

import junit.framework.TestCase;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RandomizedResponseTest extends TestCase {

    /**
     * This is a custom enum that will represent possible values
     */
    enum Level {
        HIGH,
        MEDIUM,
        LOW,
        VERY_HIGH
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    /**
     * This test that the true value is taken more than other values.
     * It also tests that other values are taken with a non-zero probability.
     * @throws Exception
     */
    public void testComputePrivateDiscreteValue() throws Exception {

        Set<Level> possibleValues = new HashSet<Level>();
        Map<Level, Integer> valueCount = new HashMap<Level, Integer>();

        for(Level level: Level.values()){
            possibleValues.add(level);
            valueCount.put(level, 0);
        }


        int numSamples = 1000;

        for(int i=0; i<numSamples; i++){

            Level value = RandomizedResponse.computePrivateDiscreteValue(Level.HIGH, possibleValues, 0.5);

            valueCount.put(value, valueCount.get(value) + 1);

        }

        int maxValue = -1;

        for (Map.Entry<Level,Integer> entry : valueCount.entrySet()) {
            Integer value = entry.getValue();
            assertTrue( value > 2);

            if(value > maxValue){
                maxValue = value;
            }
        }

        assertEquals(valueCount.get(Level.HIGH).intValue(), maxValue);

    }

    public void testComputePrivateIntegerValue() throws Exception {
    }

    public void testComputePrivateIntegerValue1() throws Exception {
    }

}