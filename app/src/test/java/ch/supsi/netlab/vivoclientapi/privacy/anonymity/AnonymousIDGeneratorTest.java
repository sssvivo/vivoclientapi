package ch.supsi.netlab.vivoclientapi.privacy.anonymity;

import junit.framework.TestCase;

import java.util.HashSet;
import java.util.Set;

public class AnonymousIDGeneratorTest extends TestCase {
    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
    }

    /**
     * Check that successive call to the AnonymousIDGenerator.getAnonymousId with
     * the same inputs give the same outputs.
     * @throws Exception
     */
    public void testGetAnonymousIdGiveSameOutputForSameInput() throws Exception {

        String id1 = "paul";
        String anonymousId1 = AnonymousIDGenerator.getAnonymousId(id1);
        String anonymousId2 = AnonymousIDGenerator.getAnonymousId(id1);

        assertEquals(anonymousId1, anonymousId2);


    }

    /**
     * Check that successive call to the AnonymousIDGenerator.getAnonymousId with
     * the different inputs give different outputs.
     * @throws Exception
     */
    public void testGetAnonymousIdGiveDifferentOutputForDifferentInput() throws Exception{

        String ids [] = {"paul", "paul2", "john"};

        Set<String> anonymousIds = new HashSet<String>();

        for (String id: ids){
            anonymousIds.add(AnonymousIDGenerator.getAnonymousId(id));
        }

        assertEquals(anonymousIds.size(), ids.length);
    }


    /**
     * Check that successive call to AnonymousIDGenerator.getUniqueId give different outputs.
     * @throws Exception
     */
    public void testGetUniqueId() throws Exception {

        Set<String> uniqueIds = new HashSet<String>();

        int numId = 5;

        for(int i=0; i<numId; i++){
            uniqueIds.add(AnonymousIDGenerator.getUniqueId());
        }

        assertEquals(uniqueIds.size(), numId);
    }

}