package ch.supsi.netlab.vivoclientapi.privacy.localdp;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// TODO: Generate unique ID as a function

/**
 * This implements the randomized response mechanism for differential privacy.
 * Created by yedtoss on 2017-10-16.
 */

public class RandomizedResponse {
    /**
     * This computes a private value using the Randomized Response Mechanism.
     * It supports any values where the set of all possible values is discrete.
     * @param trueValue The value that we want to make private.
     * @param possibleValues The set of all possible values.
     * @param privacyLoss A double indicating the privacy level
     * @param <T> The type of each value. Any Type is supported here.
     * @return A private value taken from possibleValues.
     */
    public static <T> T computePrivateDiscreteValue(T trueValue, Set<T> possibleValues, double privacyLoss){

        if (!possibleValues.contains(trueValue)){
            throw new IllegalArgumentException("The true value provided is not included in the possible values");
        }

        if(privacyLoss < 0){
            throw new IllegalArgumentException("Privacy loss can not be negative");
        }

        List<T> possibleValuesList = new ArrayList<T>(possibleValues);

        int trueValueIndex = possibleValuesList.indexOf(trueValue);

        int universeSize = possibleValues.size();

        // Set the probabilities of every possible value.
        double[] probabilities = new double[universeSize];
        for(int i=0; i<universeSize; i++){
            probabilities[i] = 1./(universeSize - 1. + Math.exp(privacyLoss));
        }
        // Set the probability of the true answer
        probabilities[trueValueIndex] = Math.exp(privacyLoss)/(universeSize - 1. + Math.exp(privacyLoss));

        // Set the values to sample
        int[] values = new int[universeSize];
        for(int i=0; i<universeSize; i++){
            values[i] = i;
        }

        // Sample a private answer from the corresponding discrete distribution
        EnumeratedIntegerDistribution distribution = new EnumeratedIntegerDistribution(values, probabilities);

        return possibleValuesList.get(distribution.sample());

    }

    /**
     * Compute a private answer using randomized response where the set of all possible values is from
     * lowestValue (included) to highestValue (included).
     * @param trueValue The value that we want to make private.
     * @param lowestValue The lowest possible integer.
     * @param highestValue The highest possible integer.
     * @param privacyLoss A double indicating the privacy level
     * @return A private value taken between lowestValue and highestValue.
     */
    public static int computePrivateIntegerValue(int trueValue, int lowestValue, int highestValue, double privacyLoss){

        Set<Integer>  possibleValues = new HashSet<Integer>();
        for(int i=lowestValue; i<=highestValue; i++){
            possibleValues.add(i);
        }

        return computePrivateDiscreteValue(trueValue, possibleValues, privacyLoss);

    }

    /**
     * Compute a private answer using randomized response where the set of all possible values is from
     * 0 (included) to highestValue (included).
     *
     * @param trueValue The value that we want to make private.
     * @param highestValue The highest possible integer.
     * @param privacyLoss A double indicating the privacy level
     * @return A private value taken between 0 and highestValue.
     */
    public static int computePrivateIntegerValue(int trueValue, int highestValue, double privacyLoss){

        return computePrivateIntegerValue(trueValue,0, highestValue, privacyLoss);
    }

}

