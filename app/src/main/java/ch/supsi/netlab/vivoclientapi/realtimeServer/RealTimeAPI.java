package ch.supsi.netlab.vivoclientapi.realtimeServer;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import ch.supsi.netlab.vivoclientapi.utils.CryptoUtils;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by felipe on 19/10/17.
 */

public class RealTimeAPI {
    public static final String TAG = RealTimeAPI.class.getSimpleName();

    public enum RequestMethod {
        POST, GET
    }


    private static String URL = null;

    private static RealTimeService service;
    private static Retrofit retrofit;

    private static Callback defaultCallback;

    public static Map<String, String> defaultHeaders;

    public static final int DEFAULT_TYPE = 0;

    static {
        defaultHeaders = new HashMap<>();
        defaultHeaders.put("Accept", "*/*");
        defaultHeaders.put("Accept-Encoding", "gzip, deflate, br");
        defaultHeaders.put("Connection", "keep-alive");
        defaultHeaders.put("Content-Type", "application/json");

        defaultCallback = new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d(TAG, "Request Headers: " + call.request().header("Authorization"));
                Log.d(TAG, "Request url: " + call.request().url());
                Log.i(TAG, "Response successful: " + response.isSuccessful());
                Log.i(TAG, "Response message: " + response.message());
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e(TAG, "Request Failed: " + t.getMessage());
            }
        };
    }

    // Internal methods

    private static Retrofit getClient() {
        if (retrofit == null && URL != null) {
            retrofit = getBuilder(URL).build();
        }

        return retrofit;
    }

    private static RealTimeService getService() {
        if (service == null) {
            service = getClient().create(RealTimeService.class);
        }
        return service;
    }

    private static Retrofit.Builder getBuilder(String url) {
        Retrofit.Builder builder = new Retrofit.Builder();
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create();
        OkHttpClient client = new OkHttpClient.Builder()
                .followRedirects(true)
                .followSslRedirects(true)
                .build();
        builder.client(client);
        builder.addConverterFactory(gsonConverterFactory);

        builder.baseUrl(url);
        return builder;
    }

    // Public interface

    public static void setEndpoint(String url) {
        URL = url;
        retrofit = null;
        service = null;
        getClient();
    }


    public static void sendData(String data, int type) {
        sendData(data, type, "/");
    }

    public static void sendData(String data, int type, String path) {
        sendData(data, type, path, defaultCallback);
    }

    public static void sendData(String data, int type, String path, Callback callback) {
        RequestBody requestBody = getRequestBody(data, type);
        Call<ResponseBody> call = getService().requestPost(defaultHeaders, path, requestBody);
        call.enqueue(callback);
    }


    public static void customRequest(RequestMethod method, Map<String, String> headers, String path, RequestBody requestBody) {

        customRequest(method, headers, path, requestBody, defaultCallback);
    }

    public static void customRequest(RequestMethod method, Map<String, String> headers, String path, RequestBody requestBody, Callback callback) {
        Call<ResponseBody> call = null;

        if (method.equals(RequestMethod.GET))
            call = getService().requestGet(headers, path, requestBody);
        else if (method.equals(RequestMethod.POST))
            call = getService().requestPost(headers, path, requestBody);

        call.enqueue(callback);
    }


    @NonNull
    public static RequestBody getRequestBody(String data, int type) {
        byte[] encrypted = CryptoUtils.compressAndEncrypt(data);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("data", CryptoUtils.encodeBytes(encrypted));
        jsonObject.addProperty("type", type);
        jsonObject.addProperty("timestamp", System.currentTimeMillis());
        jsonObject.addProperty("encrypted", true);
        return getRequestBody(jsonObject);
    }

    @NonNull
    public static RequestBody getRequestBody(JsonObject jsonObject) {
        return RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
    }

    public static String getEndPoint() {
        return URL;
    }
}
