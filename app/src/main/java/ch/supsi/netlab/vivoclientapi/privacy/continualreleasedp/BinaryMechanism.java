package ch.supsi.netlab.vivoclientapi.privacy.continualreleasedp;

import org.apache.commons.math3.distribution.LaplaceDistribution;

import ch.supsi.netlab.vivoclientapi.privacy.Utility;

/**
 * This class implements the binary mechanism (Algorithm 2) presented in
 * (Private and Continual Release of Statistics) https://eprint.iacr.org/2010/076.pdf
 */
public class BinaryMechanism {

    int horizon;
    double epsilon;
    LaplaceDistribution distribution;
    int timeStep;

    double[] alpha;
    double[] privateAlpha;

    public BinaryMechanism(int maximumNumberOfValues, double privacyLoss) {

        this.timeStep = 0;
        this.horizon = maximumNumberOfValues;
        this.epsilon = privacyLoss/Math.log(this.horizon);
        this.distribution = new LaplaceDistribution(0, 1./this.epsilon);

        this.alpha = new double[(int) Math.ceil(Utility.log2(this.horizon))];
        this.privateAlpha = new double[this.alpha.length];
    }


    public void update(double trueValue){

        // Update the time step
        this.timeStep += 1;

        // Get the minimum digit not equal to 0 in the binary form of timeStep.
        int i = (int) Utility.log2((this.timeStep & ((~this.timeStep) + 1)));

        // Update each relevant p-sums with the newly received reward
        this.alpha[i] = trueValue;
        for (int j=0; j<i; j++){
            this.alpha[i] += this.alpha[j];
        }

        // Initialize the noisy p-sums
        for (int j=0; j<i-1; j++){
            this.alpha[j] = 0;
            this.privateAlpha[j] = 0;
        }

        //Update the noisy p-sums by adding Laplace noise
        this.privateAlpha[i] = this.alpha[i] + this.distribution.sample();

    }


    /**
     * This compute the sum of the currently observed values using the Binary Mechanism.
     * @return
     */
    public double getPrivateSum(){

        double privateSum = 0;

        // Loop trough the relevant p-sum to return the current sum
        for(int j=0; j<this.privateAlpha.length; j++){
            if(((this.timeStep >> j) & 1) != 0){

                privateSum += this.privateAlpha[j];

            }
        }


        return privateSum;

    }


    public double getPrivateMean(){

        return this.getPrivateSum()/this.timeStep;
    }
}
