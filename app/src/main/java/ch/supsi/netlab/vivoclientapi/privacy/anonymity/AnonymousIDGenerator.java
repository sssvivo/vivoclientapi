package ch.supsi.netlab.vivoclientapi.privacy.anonymity;

import java.util.UUID;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * This contains method to handle anonymous ID when collecting data.
 */
public class AnonymousIDGenerator {


    /**
     * Generate a unique user identifier.
     * @return A unique user identifier. The identifier is different between successive calls.
     */
    public static String getUniqueId(){

        return UUID.randomUUID().toString();
    }

    /**
     * Given an ID, return an almost unique anonymous/encrypted identifier.
     * Here if the same ID is given to the function, it is expected to return the same identifier.
     * Two different ID are expected to generate different identifier.
     * It should be hard from the generated identifier to guess the original ID.
     *
     * This is implemented using SHA-256 hash function
     * @param id
     * @return
     */
    public static String getAnonymousId(String id){
        return  new String(Hex.encodeHex(DigestUtils.sha256(id)));
//        return DigestUtils.sha256Hex(id);

    }
}
