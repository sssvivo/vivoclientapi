package ch.supsi.netlab.vivoclientapi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import ch.supsi.netlab.vivoclientapi.utils.Callback;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * An {@link IntentService} subclass for handling asynchronous start/stop requests in
 * a service on a separate handler thread.
 * <p>
 *
 */
public class VivoService extends IntentService {

    public static final String ACTION_START = "synergy.vivo.exp.START_EXP";
    public static final String ACTION_STOP = "synergy.vivo.exp.STOP_EXP";

    private static final String TAG = VivoService.class.getSimpleName();
    private static Callback customCallback = null;


    public VivoService() {
        super("VivoService");
    }

    public static Callback getCustomCallback() {
        return customCallback;
    }

    public static void setCustomCallback(Callback customCallback) {
        VivoService.customCallback = customCallback;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent");
        if (intent != null) {
            final String action = intent.getAction();
            Log.d(TAG, "action=" + action);
            switch (action) {
                case ACTION_START:
                    handleActionStart();
                    break;
                case ACTION_STOP:
                    handleActionStop();
                    break;
            }
        }
    }


    /**
     * Handle action Start in the provided background thread with the provided
     * parameters.
     */
    private void handleActionStart() {
        Log.i(TAG, "starting experiment");

        if (customCallback != null) {
            Log.i(TAG, "custom callback: onStart calling...");
            customCallback.onStart();
        }

        String packageName = getApplicationContext().getPackageName();
        Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Handle action Stop in the provided background thread with the provided
     * parameters.
     */
    private void handleActionStop() {
        Log.i(TAG, "stopping experiment");

        if (customCallback != null) {
            Log.i(TAG, "custom callback: onStop calling...");
            customCallback.onStop();
        }

        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
