package ch.supsi.netlab.vivoclientapi.privacy.continualreleasedp;

import org.apache.commons.math3.distribution.LaplaceDistribution;

import ch.supsi.netlab.vivoclientapi.privacy.Utility;

/**
 * This class implements the Logarithm mechanism (Algorithm 3) presented in
 * (Private and Continual Release of Statistics) https://eprint.iacr.org/2010/076.pdf
 */
public class LogarithmMechanism {

    private double privateSum;
    double privacyLoss;
    int timeStep;

    LaplaceDistribution distribution;

    /**
     *
     * @param privacyLoss This is the target overall privacy.
     */
    public LogarithmMechanism(double privacyLoss) {

        privateSum = 0;
        timeStep = 0;
        this.privacyLoss = privacyLoss;
        this.distribution = new LaplaceDistribution(0, 1./this.privacyLoss);
    }


    /**
     * This method should be called for every new value collected from the user; that is for every new observation;
     * or for every new v_i for the sum \sum_i v_i.
     * @param trueValue The observed true value
     */
    public void update(double trueValue){

        privateSum += trueValue;
        timeStep += 1;

        if (Utility.isPowerOf2(timeStep)){

            privateSum += distribution.sample();

        }
    }


    /**
     * This compute the sum of the currently observed values.
     * @return the private sum using the logarithm mechanism.
     */
    public double getPrivateSum(){
        return privateSum;
    }

    /**
     *
     * @return the private mean using the logarithm mechanism.
     */
    public double getPrivateMean(){
        return privateSum/timeStep;
    }
}
