package ch.supsi.netlab.vivoclientapi.privacy.continualreleasedp;


import ch.supsi.netlab.vivoclientapi.privacy.Utility;

/**
 * This class implements the Hybrid mechanism (Algorithm 4) presented in
 * (Private and Continual Release of Statistics) https://eprint.iacr.org/2010/076.pdf.
 * It is a slightly improved version from Algorithm 4.
 */
public class HybridMechanism {

    double privacyLoss;
    int timeStep;

    LogarithmMechanism logarithmMechanism;
    BinaryMechanism binaryMechanism;

    public HybridMechanism(double privacyLoss) {

        this.privacyLoss = privacyLoss;
        this.timeStep = 0;

        this.logarithmMechanism = new LogarithmMechanism(privacyLoss);
        this.binaryMechanism = new BinaryMechanism(1, privacyLoss);
    }


    public void update(double trueValue){

        this.logarithmMechanism.update(trueValue); // Update the logarithm mechanism
        this.timeStep += 1;  // Update the current time step

        if(this.timeStep > 1){

            // If timeStep is a power of 2, create a new instance of the binary mechanism
            if(Utility.isPowerOf2(this.timeStep)){

                // We used epsilon and not epsilon/2.
                this.binaryMechanism = new BinaryMechanism(this.timeStep, privacyLoss);

            } else { // If timeStep is not a power of 2, update the binary mechanism
                this.binaryMechanism.update(trueValue);
            }
        }

    }


    /**
     * Compute private sum using the hybrid mechanism.
     * @return
     */
    public double getPrivateSum(){

        // If timeStep is a power of 2, use only the logarithm mechanism

        if(Utility.isPowerOf2(this.timeStep)){
            return this.logarithmMechanism.getPrivateSum();
        } else { // If t is not a power of 2, use both the logarithm mechanism and the binary mechanism

            return this.logarithmMechanism.getPrivateSum() + this.binaryMechanism.getPrivateSum();
        }


    }
}
