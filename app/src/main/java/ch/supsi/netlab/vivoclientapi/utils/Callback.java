package ch.supsi.netlab.vivoclientapi.utils;

/**
 * Callback interface
 */

public interface Callback {

    /**
     * Method called before launching experiment's Main Activity.
     * User can use this for initialize the experiment environment.
     */
    void onStart();

    /**
     * Method called before stopping experiment's process..
     */
    void onStop();
}
