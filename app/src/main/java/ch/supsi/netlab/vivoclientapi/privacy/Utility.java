package ch.supsi.netlab.vivoclientapi.privacy;

public class Utility {

    /**
     * Whether or not a positive integer is a power of 2.
     * @param n an integer > 0
     * @return True if n is a power of 2.
     */
    public static boolean isPowerOf2(int n){
        return (n > 0) && ((n & (n - 1)) == 0);

    }


    /**
     * Compute logarithm base 2.
     * @param x the number for which to compute the logarithm base 2.
     * @return log2(x).
     */
    public static double log2(double x){
        return Math.log(x)/Math.log(2);
    }
}
