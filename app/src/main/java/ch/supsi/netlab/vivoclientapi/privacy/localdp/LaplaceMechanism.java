package ch.supsi.netlab.vivoclientapi.privacy.localdp;

import org.apache.commons.math3.distribution.LaplaceDistribution;

/**
 * This contains a class that can generate differential private values using the Laplace Mechanism.
 */
public class LaplaceMechanism {


    /**
     * Computes a private value using the laplace mechanism
     * @param trueValue The true value to make private.
     * @param minimumPossibleValue The minimum value that one could get.
     * @param maximumPossibleValue The maximum value that one could get.
     * @param privacyLoss The desired privacy loss.
     * @return A private value achieving the desired privacy loss.
     */
    public static double computePrivateValue(double trueValue,
                                             double minimumPossibleValue,
                                             double maximumPossibleValue,
                                             double privacyLoss) {

        if(maximumPossibleValue <= minimumPossibleValue){
            throw new IllegalArgumentException("The maximum possible value is lower than the minimum");
        }

        if(privacyLoss < 0){
            throw new IllegalArgumentException("Privacy loss can not be negative");
        }

        double scale = (maximumPossibleValue-minimumPossibleValue)/privacyLoss;

        LaplaceDistribution distribution = new LaplaceDistribution(0, scale);

        return trueValue + distribution.sample();

    }

    /**
     * Computes a private value using the laplace mechanism.
     * @param trueValue The true value to make private.
     * @param maximumPossibleValue The maximum value that one could get.
     * @param privacyLoss The desired privacy loss.
     * @return A private value achieving the desired privacy loss assuming the minimum possible value is 0.
     */
    public static double computePrivateValue(double trueValue,
                                             double maximumPossibleValue,
                                             double privacyLoss) {

        return computePrivateValue(trueValue, 0, maximumPossibleValue, privacyLoss);

    }
}
