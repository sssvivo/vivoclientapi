package ch.supsi.netlab.vivoclientapi.utils;

/**
 * Created by felipe on 26/07/17.
 */

public class Constants {
    public static final String PACKAGE_NAME = "package_name";
    public static final String TIMESTAMP = "timestamp";
    public static final String DATA_TYPE = "data_type";
    public static final String DATA = "data";
    public static final String DATA_CODE = "data_code";
    public static final String VIVO_CLIENT_PACKAGE_NAME = "ch.supsi.netlab.vivoclient";

    public static final String SAVE_LOCAL = "save_local";
    public static final String LOCAL_PATH = "local_path";
    public static final String LOCAL_FILE_NAME = "vivo_collected_data.json";

    public static final String COMPRESSION = "compression";
    public static final String PUBLIC_KEY_PATH = "public_key_path";
}
