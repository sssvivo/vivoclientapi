package ch.supsi.netlab.vivoclientapi.realtimeServer;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by felipe on 19/10/17.
 */

public interface RealTimeService {
    @POST
    Call<ResponseBody> requestPost(@HeaderMap Map<String, String> values, @Url String path, @Body RequestBody body);

    @GET
    Call<ResponseBody> requestGet(@HeaderMap Map<String, String> values, @Url String path, @Body RequestBody body);
}
