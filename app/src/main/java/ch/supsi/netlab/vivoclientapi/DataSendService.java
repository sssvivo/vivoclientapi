package ch.supsi.netlab.vivoclientapi;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import ch.supsi.netlab.vivoclientapi.utils.Constants;
import ch.supsi.netlab.vivoclientapi.utils.CryptoUtils;


/**
 * An {@link IntentService} subclass for handling asynchronous data send requests in
 * a service on a separate handler thread.
 * <p>
 */
public class DataSendService extends IntentService {

    public static final String ACTION_SEND_DATA = "ch.supsi.netlab.vivoclientapi.action.SEND_DATA";
    public static final String ACTION_STORE_DATA = "ch.supsi.netlab.data.STORE_DATA";


    public DataSendService() {
        super("DataSendService");
    }

    private static final String TAG = DataSendService.class.getSimpleName();

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            switch (action) {
                case ACTION_SEND_DATA:
                    handleActionSendData(intent);
                    break;
            }

        }
    }

    public static void startActionSendData(Context context, String data, int dataType, long timestamp) {
//        byte[] bytes = data.getBytes();
        byte[] bytes = CryptoUtils.decodeString(data);
        startActionSendData(context, bytes, dataType, timestamp);
    }

    public static void startActionSendDataLocal(Context context, byte[] data, int dataType, long timestamp, String path) {
        // encrypting data
        //byte[] encrypted = CryptoUtils.encrypt(data);
        Intent intent = new Intent(context, DataSendService.class);
        intent.putExtra(Constants.PACKAGE_NAME, context.getPackageName());
        intent.putExtra(Constants.TIMESTAMP, timestamp);
        intent.putExtra(Constants.DATA_TYPE, dataType);
        intent.putExtra(Constants.DATA, data);
        //intent.putExtra(Constants.DATA, encrypted);

        intent.putExtra(Constants.SAVE_LOCAL, true);
        intent.putExtra(Constants.LOCAL_PATH, path);
        intent.setAction(ACTION_SEND_DATA);
        context.startService(intent);
    }

    public static void startActionSendData(Context context, byte[] data, int dataType, long timestamp) {
        // encrypting data
        //String encrypted = RSA.encryptWithStoredKey(data);
        //String encrypted = CryptoUtils.encryptToBase64(data);
        //byte[] encrypted = CryptoUtils.encrypt(data);
        Intent intent = new Intent(context, DataSendService.class);
        intent.putExtra(Constants.PACKAGE_NAME, context.getPackageName());
        intent.putExtra(Constants.TIMESTAMP, timestamp);
        intent.putExtra(Constants.DATA_TYPE, dataType);
        intent.putExtra(Constants.SAVE_LOCAL, false);
        //intent.putExtra("data", data.getBytes());
        //intent.putExtra("data", encrypted.getBytes());
        intent.putExtra(Constants.DATA, data);
        intent.setAction(ACTION_SEND_DATA);
        context.startService(intent);
    }


    private void handleActionSendData(Intent sendIntent) {
//        Log.d(TAG, "sending data to client");
        String packageName = sendIntent.getStringExtra(Constants.PACKAGE_NAME);
        long timestamp = sendIntent.getLongExtra(Constants.TIMESTAMP, 0);
        int dataType = sendIntent.getIntExtra(Constants.DATA_TYPE, 0);
        byte[] data = sendIntent.getByteArrayExtra(Constants.DATA);
        byte[] compressed = null;
        boolean compression = true;


        SharedPreferences cryptoPrefs = CryptoUtils.getCryptoPrefs();

        if (cryptoPrefs != null) {
            compression = cryptoPrefs.getBoolean(Constants.COMPRESSION, true);
        }

        if (compression) {
            compressed = CryptoUtils.compress(data);
        }

        byte[] encrypted;

        if (compressed != null)
            encrypted = CryptoUtils.encrypt(compressed);
        else
            encrypted = CryptoUtils.encrypt(data);

        Log.d(TAG, "handleActionSendData encrypted data: " + CryptoUtils.encodeBytes(encrypted));

        boolean saveLocal = sendIntent.getBooleanExtra(Constants.SAVE_LOCAL, false);


        if (saveLocal) {
            //String base64Data = Base64.encodeToString(data, Base64.DEFAULT);
            String base64Data = Base64.encodeToString(encrypted, Base64.DEFAULT);
            String localPath = sendIntent.getStringExtra(Constants.LOCAL_PATH);
            Log.d(TAG, "Saving to local file at: " + localPath);

            File outputDir = new File(localPath);
            outputDir.mkdir();

            FileOutputStream outputStream = null;

            try {
                File outputFile = new File(outputDir, Constants.LOCAL_FILE_NAME);

                outputStream = new FileOutputStream(outputFile.getAbsolutePath(), true);

                JSONObject newData = new JSONObject();
                newData.put(Constants.TIMESTAMP, timestamp);
                newData.put(Constants.DATA_TYPE, dataType);
                newData.put(Constants.DATA, base64Data);
                outputStream.write(newData.toString(2).getBytes());
                outputStream.write(", \n".getBytes());

                outputStream.close();

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

        } else {
            //byte[] encrypted = CryptoUtils.encrypt(data);
            Intent intent = new Intent(ACTION_STORE_DATA);
            intent.putExtra(Constants.PACKAGE_NAME, packageName);
            intent.putExtra(Constants.TIMESTAMP, timestamp);
            intent.putExtra(Constants.DATA_CODE, dataType);
            intent.putExtra(Constants.DATA, encrypted);
//            intent.putExtra(Constants.DATA, data);
            intent.setPackage(Constants.VIVO_CLIENT_PACKAGE_NAME);
            startService(intent);
        }
    }


    public static boolean checkPermission(String permission, Context context) {
        PackageManager packageManager = context.getPackageManager();
        return (packageManager.checkPermission(permission, Constants.PACKAGE_NAME) == PackageManager.PERMISSION_GRANTED);
    }



}
