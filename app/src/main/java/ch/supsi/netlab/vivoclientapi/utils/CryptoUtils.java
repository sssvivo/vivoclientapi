package ch.supsi.netlab.vivoclientapi.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.KeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import javax.crypto.Cipher;


/**
 * Utility class used for RSA asymmetric cryptography
 * It can be used for encrypt and decrypt data using a key pair (public and private).
 * Keys must be in .der format files. For encryption only, it requires only public key file.
 */
public class CryptoUtils {
    private static final String TAG = CryptoUtils.class.getSimpleName();

    //    private static final String CIPHER_ALGORITHM = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";
    public static final String CHARSET_NAME = "UTF-8";

    private static final String CIPHER_ALGORITHM = "RSA/ECB/PKCS1Padding";

    private static final String CRYPTO_PREFERENCES = "CRYPTO_PREFS";

//    private static final String PRIVATE_KEY_DEFAULT_NAME ="private_key.der";
    private static final String PUBLIC_KEY_DEFAULT_NAME ="public_key.der";

    private static Key publicKey = null;
    private static Key privateKey = null;
    private static SharedPreferences cryptoPrefs = null;


    public static void initializeCryptoUtils(Context context, boolean compression){
        initializeCryptoUtils(context, PUBLIC_KEY_DEFAULT_NAME, compression);
    }

    public static void initializeCryptoUtils(Context context, String publicKeyPath, boolean compression){
        cryptoPrefs = context.getSharedPreferences(CRYPTO_PREFERENCES, Context.MODE_PRIVATE);
        cryptoPrefs.edit().putBoolean(Constants.COMPRESSION, compression).apply();
        cryptoPrefs.edit().putString(Constants.PUBLIC_KEY_PATH, publicKeyPath).apply();
        setPublicKeyFromAssets(context, publicKeyPath);
    }

    public static SharedPreferences getCryptoPrefs() {
        return cryptoPrefs;
    }

    public static String getKeyFromAssets(Context context, String filePath) {
        File cacheFile = new File(context.getCacheDir(), filePath);
        try {
            InputStream inputStream = context.getAssets().open(filePath);
            try {
                FileOutputStream outputStream = new FileOutputStream(cacheFile);
                try {
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = inputStream.read(buf)) > 0) {
                        outputStream.write(buf, 0, len);
                    }
                } finally {
                    outputStream.close();
                }
            } finally {
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cacheFile.getAbsolutePath();
        //setPublicKey(cacheFile.getAbsolutePath());
    }

    public static void setPublicKeyFromAssets(Context context, String fileName) {
        String keyPath = getKeyFromAssets(context, fileName);
        setPublicKey(keyPath);
    }

    public static void setPrivateKeyFromAssets(Context context, String fileName) {
        String keyPath = getKeyFromAssets(context, fileName);
        setPrivateKey(keyPath);
    }


    public static void setPublicKey(String filePath) {
        File f = new File(filePath);
        byte[] keyBytes = getBytes(f);
        try {
            X509EncodedKeySpec spec =
                    new X509EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");

            publicKey = kf.generatePublic(spec);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    public static void setPrivateKey(String filePath) {
        File f = new File(filePath);
        byte[] keyBytes = getBytes(f);
        try {
            PKCS8EncodedKeySpec spec =
                    new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            privateKey = kf.generatePrivate(spec);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    public static String encryptToBase64(String toBeCiphred) {
        return encryptToBase64(publicKey, toBeCiphred);
    }

    public static String encryptToBase64(Key publicKey, String toBeCiphred) {
        try {
            byte[] ciphered = encrypt(publicKey, toBeCiphred.getBytes());

            return encodeBytes(ciphered);
        } catch (Exception e) {
            Log.e(TAG, "Error while encrypting data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }


    public static byte[] encrypt(Key publicKey, byte[] toBeCiphred) {
        try {
            Cipher rsaCipher = Cipher.getInstance(CIPHER_ALGORITHM);
            Log.d(TAG, "encrypt with: " + rsaCipher.getAlgorithm());
            rsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return rsaCipher.doFinal(toBeCiphred);
        } catch (Exception e) {
            Log.e(TAG, "Error while encrypting data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static byte[] encrypt(byte[] toBeCiphred) {
        if (publicKey == null)
            try {
                throw new KeyException("Public key not initialized!");
            } catch (KeyException e) {
                e.printStackTrace();
            }
        return encrypt(publicKey, toBeCiphred);
    }


    public static String decryptFromBase64(String encryptedText) {
        return decryptFromBase64(privateKey, encryptedText);
    }

    public static String decryptFromBase64(Key privateKey, String encryptedText) {
        try {
//            Cipher rsaCipher = Cipher.getInstance("RSA");
//            rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] deciphered = decrypt(privateKey, decodeString(encryptedText));
            return stringify(deciphered);
        } catch (Exception e) {
            Log.e(TAG, "Error while decrypting data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }


    public static String encodeBytes(byte[] bytes) {

        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public static byte[] decodeString(String string) {
        return Base64.decode(string, Base64.NO_WRAP);
    }

    public static byte[] decrypt(byte[] encryptedData) {
        if (privateKey == null) {
            try {
                throw new KeyException("Private key not initialized!");
            } catch (KeyException e) {
                e.printStackTrace();
            }
        }
        return decrypt(privateKey, encryptedData);
    }

    public static byte[] decrypt(String encryptedText) {
        byte[] decoded = decodeString(encryptedText);
        return decrypt(privateKey, decoded);
    }



    public static byte[] decrypt(Key privateKey, byte[] encryptedData) {
        try {
            Cipher rsaCipher = Cipher.getInstance(CIPHER_ALGORITHM);
            Log.d(TAG, "decrypt with: " + rsaCipher.getAlgorithm());
            rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);
            return rsaCipher.doFinal(encryptedData);
        } catch (Exception e) {
            Log.e(TAG, "Error while decrypting data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private static byte[] getBytes(File f) {
        byte[] keyBytes = null;
        FileInputStream fis;
        try {
            fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            keyBytes = new byte[(int) f.length()];
            dis.readFully(keyBytes);
            dis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return keyBytes;
    }

    public static String stringify(byte[] bytes) {
        return stringify(new String(bytes));
    }

    private static String stringify(String str) {
        String aux = "";
        for (int i = 0; i < str.length(); i++) {
            aux += str.charAt(i);
        }
        return aux;
    }

    public static String getString(byte[] bytes) throws UnsupportedEncodingException {
        return new String(bytes, CHARSET_NAME);
    }

    public static byte[] getBytes(String s) throws UnsupportedEncodingException {
        return s.getBytes(CHARSET_NAME);

    }

    public static byte[] compress(byte[] bytes) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            OutputStream out = new DeflaterOutputStream(baos);
            out.write(bytes);
            out.close();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        return baos.toByteArray();
    }

    public static byte[] compressAndEncrypt(String data){
        try {
            byte[] bytes = CryptoUtils.getBytes(data);
            return compressAndEncrypt(bytes);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] compressAndEncrypt(byte[] data){
        byte[] compressed = compress(data);
        return encrypt(compressed);
    }


    public static byte[] compress(String text) throws UnsupportedEncodingException {
        return compress(text.getBytes(CHARSET_NAME));
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        try {
//            byte[] bytes = text.getBytes(CHARSET_NAME);
//            OutputStream out = new DeflaterOutputStream(baos);
//            out.write(bytes);
//            out.close();
//        } catch (IOException e) {
//            throw new AssertionError(e);
//        }
//        return baos.toByteArray();
    }

    public static String decompress(byte[] bytes) {
        InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[8192];
            int len;
            while ((len = in.read(buffer)) > 0)
                baos.write(buffer, 0, len);
            return new String(baos.toByteArray(), CHARSET_NAME);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

}
